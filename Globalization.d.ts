export interface StringMap {
    InputPlaceHolder?: string;
    TypingIndicator?: string;
    Connecting?: string;
    Disconnected?: string;
    Read?: string;
    MessageSendingDisabled?: string;
    Today?: string;
    Yesterday?: string;
}
export interface LanguageMap {
    [languageKey: string]: StringMap;
}
/**
 * Globalization class. Used to define translation for languages.
 */
export declare class Globalization {
    private static instance;
    /**
     * Default language code.
     * @readonly
     * @default
     */
    static readonly DEFAULT_LANGUAGE: string;
    /**
     * Returns a singleton instance of the class.
     * @returns {Globalization}
     */
    static getInstance(): Globalization;
    private language;
    private overrides;
    private strings;
    /**
     * Private
     *
     * @ignore
     */
    private constructor();
    /**
     * Sets the active language.
     *
     * @param {string} language The code of the langauge in use, for example: "en-us"
     * @memberof Globalization
     */
    setLanguage(language: string): void;
    /**
     * Sets value for string key for given language.
     *
     * @param {string} key Key of the string map. See {@link Globalization#StringMap} for key values.
     * @param {string} value Localized value for the key
     * @param {string} [language=Globalization.DEFAULT_LANGUAGE] The code of the langauge in use, for example: "en-us"
     */
    setString(key: string, value: string, language?: string): void;
    /**
     * Sets the entire set of strings for given language.
     *
     * @param {Globalization#StringMap} strings String map for the language
     * @param {string} [language=Globalization.DEFAULT_LANGUAGE] The code of the langauge in use, for example: "en-us"
     */
    setLanguageStrings(strings: StringMap, language?: string): void;
    /**
     * Sets the entire language map with all strings.
     *
     * @param {LanguageMap} languageStrings Map of all languages and strings
     * @memberof Globalization
     */
    setAllStrings(languageStrings: LanguageMap): void;
    /**
     * Gets a string map for currently active language.
     * Language tokens that are missing from the language will be initialized from default language.
     *
     * @returns {Globalization#StringMap}
     */
    getStrings(): StringMap;
    private initializeStrings();
    private invalidateStrings();
}
/**
 * Gets a string map for currently active language.
 *
 * @returns {Globalization#StringMap}
 * @ignore
 */
export declare function strings(): StringMap;
