"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    InputPlaceHolder: "Kirjuta sõnum siia",
    TypingIndicator: "{0} kirjutab … ",
    Connecting: "Ühendab …",
    Disconnected: "Ühendust pole",
    Read: "Loetud",
    MessageSendingDisabled: "Sõnumite saatmine pole hetkel lubatud",
    Today: "Täna",
    Yesterday: "Eile"
};
