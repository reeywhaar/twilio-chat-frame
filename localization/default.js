"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var defaultStrings = {
    InputPlaceHolder: "Type message",
    TypingIndicator: "{0} is typing … ",
    Connecting: "Connecting …",
    Disconnected: "Connection lost",
    Read: "Read",
    MessageSendingDisabled: "Message sending has been disabled",
    Today: "Today",
    Yesterday: "Yesterday"
};
exports.defaultStrings = defaultStrings;
