/// <reference types="react" />
import * as React from "react";
export interface Props {
    style?: Object;
}
declare class CloseIcon extends React.PureComponent<Props, undefined> {
    private _style;
    constructor(props: Props);
    render(): JSX.Element;
}
declare class SendIcon extends React.PureComponent<Props, undefined> {
    private _style;
    constructor(props: Props);
    render(): JSX.Element;
}
declare class DudeIcon extends React.PureComponent<Props, undefined> {
    private _style;
    constructor(props: Props);
    render(): JSX.Element;
}
declare class ReadIcon extends React.PureComponent<Props, undefined> {
    private _style;
    constructor(props: Props);
    render(): JSX.Element;
}
declare class ConnectionLostIcon extends React.PureComponent<undefined, undefined> {
    render(): JSX.Element;
}
export { CloseIcon, SendIcon, DudeIcon, ReadIcon, ConnectionLostIcon };
