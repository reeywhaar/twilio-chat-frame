"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_redux_1 = require("react-redux");
var Channel_1 = require("../components/channel/Channel");
// Return a channel that is connected to the store
exports.ConnectedChannel = react_redux_1.connect(function (store, ownProps) { return ({ channel: store.channels[ownProps.sid], session: store.session }); })(Channel_1.Channel);
