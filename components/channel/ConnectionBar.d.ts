/// <reference types="react" />
import * as React from "react";
import "./ConnectionBar.less";
export interface Props {
    readonly connectionState: string;
}
export default class ConnectionBar extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    render(): JSX.Element;
}
