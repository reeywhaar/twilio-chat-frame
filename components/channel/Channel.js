"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
require("./Channel.less");
var AppConfig = require("../../state/ChatConfig");
var ChannelState = require("../../state/ChannelState");
var Header_1 = require("./Header");
var MessageList_1 = require("./MessageList");
var Chrome_1 = require("./Chrome");
var ConnectionBar_1 = require("./ConnectionBar");
var Input_1 = require("./Input");
var Channel = (function (_super) {
    __extends(Channel, _super);
    function Channel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._mainAreaStyleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.main; });
        _this.setMessageListRef = function (element) {
            _this._messageList = element;
        };
        _this.handleMessageListLoadMoreMessages = function () {
            ChannelState.Actions.loadMessagesFromHistory(_this.props.channel);
        };
        _this.handleConsumeMessageAtIndex = function (index) {
            if (_this.props.channel.source.lastConsumedMessageIndex != index)
                ChannelState.Actions.updateLastConsumedMessageIndex(_this.props.channel, index);
        };
        _this.handleInputRowCountChanged = function () {
            _this._messageList.invalidateScrollPosition();
        };
        _this.handleInputMessageSent = function () {
            _this._messageList.scrollToBottom();
        };
        _this.handleInputFocus = function () {
            setTimeout(function () {
                _this._messageList.invalidateScrollPosition();
            }, 200);
        };
        _this.handleWindowResize = function () {
            _this._messageList.invalidateScrollPosition();
        };
        return _this;
    }
    Channel.prototype.render = function () {
        if (!this.props.channel)
            return React.createElement("div", null);
        return (React.createElement("div", null,
            this.renderChrome(),
            this.renderHeader(),
            React.createElement("div", { className: "Main", style: this._mainAreaStyleConfig },
                React.createElement(ConnectionBar_1.default, { connectionState: this.props.session.connectionState }),
                React.createElement("div", { className: "MessageList" },
                    React.createElement(MessageList_1.default, { ref: this.setMessageListRef, isLoadingMessages: this.props.channel.isLoadingMessages, isAppActive: this.props.session.isActive, messages: this.props.channel.messages, members: this.props.channel.members, typers: this.props.channel.typers, lastConsumedMessageIndex: this.props.channel.lastConsumedMessageIndex, onLoadMoreMessages: this.handleMessageListLoadMoreMessages, onConsumeMessageAtIndex: this.handleConsumeMessageAtIndex })),
                React.createElement("div", { className: "Input" },
                    React.createElement(Input_1.default, { channel: this.props.channel, session: this.props.session, onFocus: this.handleInputFocus, onRowCountChanged: this.handleInputRowCountChanged, onMessageSent: this.handleInputMessageSent })))));
    };
    Channel.prototype.componentDidMount = function () {
        window.addEventListener("resize", this.handleWindowResize);
    };
    Channel.prototype.componentWillUnmount = function () {
        window.removeEventListener("resize", this.handleWindowResize);
    };
    Channel.prototype.renderChrome = function () {
        if (AppConfig.current().channel.windowControls.visible) {
            return (React.createElement("div", { className: "Chrome" },
                React.createElement(Chrome_1.default, { channelSid: this.props.channel.source.sid })));
        }
    };
    Channel.prototype.renderHeader = function () {
        if (AppConfig.current().channel.header.visible) {
            return (React.createElement("div", { className: "Header" },
                React.createElement(Header_1.default, { channelName: this.props.channel.source.friendlyName })));
        }
    };
    return Channel;
}(React.PureComponent));
Channel.displayName = "Channel";
Channel = __decorate([
    pacomo_1.pacomoDecorator
], Channel);
exports.Channel = Channel;
