"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
var Globalization_1 = require("../../Globalization");
var Icons_1 = require("../icons/Icons");
var react_transition_group_1 = require("react-transition-group");
require("./ConnectionBar.less");
var ConnectionBar = ConnectionBar_1 = (function (_super) {
    __extends(ConnectionBar, _super);
    function ConnectionBar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ConnectionBar.prototype.render = function () {
        var text;
        var className;
        var show = true;
        switch (this.props.connectionState) {
            case "connecting":
                text = Globalization_1.strings().Connecting;
                className = "Connecting";
                break;
            case "connected":
                show = false;
                break;
            default:
                text = Globalization_1.strings().Disconnected;
                className = "Disconnected";
                break;
        }
        return (React.createElement(react_transition_group_1.CSSTransitionGroup, { transitionName: pacomo_1.packageName + "-" + ConnectionBar_1.displayName, transitionEnterTimeout: 300, transitionLeaveTimeout: 300 }, show &&
            React.createElement("div", { className: "Container " + className },
                React.createElement("div", { className: "Description" },
                    React.createElement(Icons_1.ConnectionLostIcon, null),
                    React.createElement("span", { className: "Text" }, text)),
                React.createElement("div", { className: "LowerLine" }))));
    };
    return ConnectionBar;
}(React.PureComponent));
ConnectionBar.displayName = "ConnectionBar";
ConnectionBar = ConnectionBar_1 = __decorate([
    pacomo_1.pacomoDecorator
], ConnectionBar);
exports.default = ConnectionBar;
var ConnectionBar_1;
