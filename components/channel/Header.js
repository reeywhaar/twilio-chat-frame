"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
var AppConfig = require("../../state/ChatConfig");
require("./Header.less");
var Header = (function (_super) {
    __extends(Header, _super);
    function Header() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._styleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.header; });
        return _this;
    }
    Header.prototype.render = function () {
        var headerConfig = AppConfig.current().channel.header;
        if (!headerConfig.image.visible && !headerConfig.title.visible)
            return React.createElement("div", null);
        var channelName = this.props.channelName || headerConfig.title.default;
        return (React.createElement("div", { className: "Container", style: this._styleConfig },
            headerConfig.image.visible &&
                React.createElement("div", { className: "Logo" },
                    React.createElement("div", { className: "LogoWrapper" },
                        React.createElement("img", { className: "LogoImage", src: headerConfig.image.url }))),
            headerConfig.title.visible &&
                React.createElement("div", { className: "Content" },
                    React.createElement("p", { className: "Text FirstLine", style: this._styleConfig }, channelName))));
    };
    return Header;
}(React.PureComponent));
Header.displayName = "ChannelHeader";
Header = __decorate([
    pacomo_1.pacomoDecorator
], Header);
exports.default = Header;
