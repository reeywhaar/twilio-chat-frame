"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var AppConfig = require("../../state/ChatConfig");
var pacomo_1 = require("../../utils/pacomo");
var Icons_1 = require("../icons/Icons");
require("./Chrome.less");
var Chrome = (function (_super) {
    __extends(Chrome, _super);
    function Chrome() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._styleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.header; });
        _this.closeClick = function () {
            var callback = AppConfig.current().channel.windowControls.closeCallback;
            if (callback) {
                callback(_this.props.channelSid);
            }
        };
        return _this;
    }
    Chrome.prototype.render = function () {
        return (React.createElement("div", { style: this._styleConfig },
            React.createElement("div", { className: "Close", onClick: this.closeClick },
                React.createElement(Icons_1.CloseIcon, { style: this._styleConfig }))));
    };
    return Chrome;
}(React.PureComponent));
Chrome.displayName = "Chrome";
Chrome = __decorate([
    pacomo_1.pacomoDecorator
], Chrome);
exports.default = Chrome;
