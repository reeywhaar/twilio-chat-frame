/// <reference types="react" />
import * as React from "react";
import * as ChannelState from "../../state/ChannelState";
import "./MessageList.less";
export interface Props {
    isLoadingMessages: boolean;
    messages: Array<ChannelState.MessageState>;
    members: ChannelState.MembersType;
    typers: Array<ChannelState.MemberState>;
    lastConsumedMessageIndex: number;
    onLoadMoreMessages: Function;
    onConsumeMessageAtIndex: Function;
    isAppActive: boolean;
}
export default class MessageList extends React.Component<Props, undefined> {
    static readonly displayName: string;
    private messageList;
    private loadedMessagesInUpdate;
    private keepScrolledToBottom;
    private keepTopScrollPosition;
    private lastScrollHeight;
    private lastScrollTop;
    private isTouchActive;
    private isScrollActive;
    private needsForceRenderAfterScroll;
    private afterScrollTimer;
    constructor(props: Props);
    componentWillUpdate(nextProps: Props, nextState: undefined): void;
    shouldComponentUpdate(nextProps: Props, nextState: undefined, nextContext: any): boolean;
    componentDidUpdate(): void;
    invalidateScrollPosition(): void;
    scrollToBottom(): void;
    private consumeLastMessageIfApplicable();
    private captureBottomScrollState();
    private captureScrollState();
    private loadMoreMessages();
    private onAfterScrollTimer;
    private scheduleAfterScrollTimer();
    private onScroll;
    private setMessageListRef;
    private findMyLastMessageIndex();
    private onTouchStart;
    private onTouchEnd;
    render(): JSX.Element;
    private renderTypingIndicator();
    private isScrolledIntoView(el);
    private injectDateSeparatorElements(messageElements);
}
