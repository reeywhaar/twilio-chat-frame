/// <reference types="react" />
import * as React from "react";
import { MessageState, MemberState } from "../../state/ChannelState";
import "./ChannelItem.less";
export interface Props {
    author: MemberState;
    message: MessageState;
    showReadStatus: boolean;
}
export default class ChannelItem extends React.PureComponent<Props, undefined> {
    static readonly displayName: string;
    private _avatarStyleConfig;
    private _bubblePrimaryStyleConfig;
    private _bubbleSecondaryStyleConfig;
    private _readStatusStyleConfig;
    private _rootElementRef;
    constructor(props: Props);
    private getClassName();
    private renderAvatarArea();
    private renderBody(rawBody);
    render(): JSX.Element;
    componentDidUpdate(prevProps: Readonly<Props>, prevState: undefined, prevContext: any): void;
    componentDidMount(): void;
    private invokeRenderCallback();
    private setRootElementRef;
}
