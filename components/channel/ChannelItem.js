"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var pacomo_1 = require("../../utils/pacomo");
var AppConfig = require("../../state/ChatConfig");
var Icons_1 = require("../icons/Icons");
var Utils = require("../../utils/utils");
var Globalization_1 = require("../../Globalization");
require("./ChannelItem.less");
var ChannelItem = (function (_super) {
    __extends(ChannelItem, _super);
    function ChannelItem(props) {
        var _this = _super.call(this, props) || this;
        _this._readStatusStyleConfig = AppConfig.tryGet(function () { return AppConfig.current().channel.visual.override.readStatus; });
        _this.setRootElementRef = function (e) {
            _this._rootElementRef = e;
        };
        _this._avatarStyleConfig = AppConfig.tryGet(function () { return props.message.isFromMe ?
            AppConfig.current().channel.visual.override.outgoingMessage.avatar :
            AppConfig.current().channel.visual.override.incomingMessage.avatar; });
        _this._bubblePrimaryStyleConfig = AppConfig.tryGet(function () { return props.message.isFromMe ?
            AppConfig.current().channel.visual.override.outgoingMessage.body :
            AppConfig.current().channel.visual.override.incomingMessage.body; });
        _this._bubbleSecondaryStyleConfig = AppConfig.tryGet(function () { return props.message.isFromMe ?
            AppConfig.current().channel.visual.override.outgoingMessage.info :
            AppConfig.current().channel.visual.override.incomingMessage.info; });
        if (!!_this._bubblePrimaryStyleConfig && !_this._bubbleSecondaryStyleConfig) {
            _this._bubbleSecondaryStyleConfig = _this._bubblePrimaryStyleConfig;
        }
        return _this;
    }
    ChannelItem.prototype.getClassName = function () {
        var className = this.props.message.isFromMe ? "fromMe" : "fromOthers";

        if (this.props.message.isFromAdmin)
            className = className + " " + "fromAdmin";
        if (this.props.message.groupWithNext)
            className = className + " " + "GroupWithNext";
        if (this.props.message.groupWithPrevious)
            className = className + " " + "GroupWithPrevious";
        className = className + " " + AppConfig.current().channel.visual.messageStyle;

        return className;
    };
    ChannelItem.prototype.renderAvatarArea = function () {
        if (AppConfig.current().channel.visual.messageStyle == "Minimal") {
            return (React.createElement("div", { className: "AvatarLine", style: this._avatarStyleConfig }));
        }
        var avatarUrl = !!AppConfig.current().avatarCallback ?
            AppConfig.current().avatarCallback(this.props.message.source.author) : undefined;
        var avatarImage = avatarUrl ? React.createElement("img", { src: avatarUrl, style: this._avatarStyleConfig }) : React.createElement(Icons_1.DudeIcon, { style: this._avatarStyleConfig });
        return (React.createElement("div", { className: "Avatar", style: this._avatarStyleConfig }, avatarImage));
    };
    ChannelItem.prototype.renderBody = function (rawBody) {
        var results = [];
        // trim as we are not interested in incoming starting and ending whitespaces
        rawBody = rawBody.trim();
        // break content into lines by line breaks, so we can add <br> for breaks
        var lineBreakRegexp = /[\n\r]/;
        var lines = rawBody.split(lineBreakRegexp);
        var urlRegexp = Utils.urlRegexp;
        lines.forEach(function (line, lineIndex) {
            var index = 0;
            var regRes;
            while (regRes = urlRegexp.exec(line)) {
                results.push(line.substr(index, regRes.index - index));
                var url = regRes[0];
                var urlHref = url;
                // if without protocol, add one
                if (!Utils.simpleUrlRegexp.exec(url)) {
                    urlHref = "http://" + url;
                }

                results.push(React.createElement("a", { href: urlHref, key: lineIndex + '' + index, target: "_blank" }, [url]));
                index = regRes.index + url.length;
            }
            if (index < line.length) {
                results.push(line.substr(index, line.length));
            }
            // add line break;
            if (lineIndex < lines.length - 1)
                results.push(React.createElement("br", { key: lineIndex }));
        });
        return results;
    };
    ChannelItem.prototype.render = function () {
        var authorName = Utils.getNameForMember(this.props.message.source.author, this.props.author);
        return (React.createElement("div", { className: this.getClassName(), ref: this.setRootElementRef },
            React.createElement("div", { className: "ContentContainer" },
                React.createElement("div", { className: "AvatarContainer" }, this.renderAvatarArea()),
                React.createElement("div", { className: "MessageContainer" },
                    React.createElement("div", { className: "BubbleContainer" },
                        React.createElement("div", { className: "Bubble", style: this._bubblePrimaryStyleConfig },
                            React.createElement("div", { className: "Header", style: this._bubbleSecondaryStyleConfig },
                                React.createElement("div", { className: "UserName", style: this._bubbleSecondaryStyleConfig },
                                    React.createElement("p", { style: this._bubbleSecondaryStyleConfig }, authorName)),
                                React.createElement("div", { className: "Time", style: this._bubbleSecondaryStyleConfig },
                                    React.createElement("p", { style: this._bubbleSecondaryStyleConfig }, this.props.message.source.timestamp.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" })))),
                            React.createElement("div", { className: "Body", style: this._bubblePrimaryStyleConfig },
                                React.createElement("p", { style: this._bubblePrimaryStyleConfig }, this.renderBody(this.props.message.source.body)))),
                        AppConfig.current().channel.visual.messageStyle === "Squared" && !this.props.message.groupWithNext &&
                            React.createElement(Spike, { reverse: this.props.message.isFromMe, style: this._bubblePrimaryStyleConfig })))),
            this.props.showReadStatus &&
                React.createElement("div", { className: "ReadStatus" },
                    React.createElement("span", { style: this._readStatusStyleConfig }, Globalization_1.strings().Read),
                    React.createElement(Icons_1.ReadIcon, { style: this._readStatusStyleConfig }))));
    };
    ChannelItem.prototype.componentDidUpdate = function (prevProps, prevState, prevContext) {
        this.invokeRenderCallback();
    };
    ChannelItem.prototype.componentDidMount = function () {
        this.invokeRenderCallback();
    };
    ChannelItem.prototype.invokeRenderCallback = function () {
        var cb = AppConfig.current().channel.message.renderCallback;
        if (cb) {
            cb(this.props.message.source, this._rootElementRef);
        }
    };
    return ChannelItem;
}(React.PureComponent));
ChannelItem.displayName = "ChatItem";
ChannelItem = __decorate([
    pacomo_1.pacomoDecorator
], ChannelItem);
exports.default = ChannelItem;
var Spike = (function (_super) {
    __extends(Spike, _super);
    function Spike(props) {
        var _this = _super.call(this, props) || this;
        if (!props.style)
            return _this;
        if (!props.style.hasOwnProperty("background"))
            return _this;
        _this._style = { "fill": props.style["background"] };
        return _this;
    }
    Spike.prototype.render = function () {
        return (React.createElement("svg", { width: "8px", height: "8px", viewBox: "0 0 8 8", version: "1.1", xmlns: "http://www.w3.org/2000/svg", style: this._style },
            React.createElement("g", { id: "Page-1", stroke: "none", strokeWidth: "1", fillRule: "evenodd" }, this.props.reverse ? (React.createElement("polygon", { id: "Rectangle", transform: "translate(4.000000, 4.000000) scale(-1, 1) translate(-4.000000, -4.000000) ", points: "0 0 8 0 4 4 0 8" })) : (React.createElement("polygon", { id: "Rectangle", points: "0 0 8 0 4 4 0 8" })))));
    };
    return Spike;
}(React.PureComponent));
Spike.displayName = "ChatItem-Spike";
Spike = __decorate([
    pacomo_1.pacomoDecorator
], Spike);
