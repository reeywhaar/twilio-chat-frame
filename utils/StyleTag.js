"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var StyleTag = (function (_super) {
    __extends(StyleTag, _super);
    function StyleTag(props) {
        var _this = _super.call(this, props) || this;
        _this._styleElement = document.createElement("style");
        _this._styleElement.type = "text/css";
        _this._styleElement.appendChild(document.createTextNode(_this.props.content));
        return _this;
    }
    StyleTag.prototype.componentDidMount = function () {
        document.head.appendChild(this._styleElement);
    };
    StyleTag.prototype.componentWillUnmount = function () {
        document.head.removeChild(this._styleElement);
    };
    StyleTag.prototype.render = function () {
        return undefined;
    };
    return StyleTag;
}(React.PureComponent));
exports.StyleTag = StyleTag;
