/// <reference types="react" />
import * as React from "react";
export interface Props {
    content: string;
}
export declare class StyleTag extends React.PureComponent<Props, undefined> {
    private _styleElement;
    constructor(props: Props);
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): any;
}
