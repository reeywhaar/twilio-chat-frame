import { MemberState } from "../state/ChannelState";
export declare function getNameForMember(identity: string, member: MemberState): string;
export declare function formatString(theString: string, ...params: string[]): string;
export declare function merge<T>(obj1: any, obj2: any): T;
export declare function createSvgStyle(style: Object, colorPropName?: string): Object;
export interface Version {
    major: number;
    minor: number;
    build: number;
}
export declare function parseVersion(version: string): Version;
export declare function isGreaterThanTargetVersion(version: Version, targetVersion: Version): boolean;
export declare function getSdkVersion(): Version;
export declare const simpleUrlRegexp: RegExp;
export declare const urlRegexp: RegExp;
export declare function copyMap<K, V>(sourceMap: Map<K, V>): Map<K, V>;
export declare function isSameDate(date1: Date, date2: Date): boolean;
export declare function isToday(date: Date): boolean;
export declare function isYesterday(date: Date): boolean;
