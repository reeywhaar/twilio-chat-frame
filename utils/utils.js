"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var twilio_chat_1 = require("twilio-chat");
var SessionState = require("../state/SessionState");
var AppConfig = require("../state/ChatConfig");
function getNameForMember(identity, member) {
    var you = SessionState.current().client.user.identity == identity;
    var customName = you ? AppConfig.current().channel.message.yourDefaultName : AppConfig.current().channel.message.theirDefaultName;
    var friendlyName = "";
    if (member)
        friendlyName = member.friendlyName;
    var friendlyNameOverride = you ? AppConfig.current().channel.message.yourFriendlyNameOverride : AppConfig.current().channel.message.theirFriendlyNameOverride;
    if (friendlyName && friendlyNameOverride)
        return friendlyName;
    else if (customName)
        return customName;
    else if (friendlyName)
        return friendlyName;
    else
        return identity;
}
exports.getNameForMember = getNameForMember;
function formatString(theString) {
    var params = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        params[_i - 1] = arguments[_i];
    }
    // start with the second argument (i = 1)
    for (var i = 0; i < params.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i) + "\\}", "gm");
        theString = theString.replace(regEx, params[i]);
    }
    return theString;
}
exports.formatString = formatString;
// Deep merge for two JSON objects. Returns a new object by overriding properties in obj1 by properties obj2.
function merge(obj1, obj2) {
    var newObject = __assign({}, obj1);
    for (var key in obj2) {
        var val = obj2[key];
        if (typeof (val) === "object") {
            val = merge(obj1[key] || {}, val);
        }
        newObject[key] = val;
    }
    return newObject;
}
exports.merge = merge;
function createSvgStyle(style, colorPropName) {
    if (colorPropName === void 0) { colorPropName = "stroke"; }
    if (style === undefined)
        return undefined;
    if (!style.hasOwnProperty("color"))
        return style;
    return __assign({}, style, (_a = {}, _a[colorPropName] = style["color"], _a));
    var _a;
}
exports.createSvgStyle = createSvgStyle;
function parseVersion(version) {
    if (!version)
        return undefined;
    var v = version.split(/[\.\+-]/);
    if (v.length < 3)
        return undefined;
    return {
        major: parseInt(v[0]),
        minor: parseInt(v[1]),
        build: parseInt(v[2]),
    };
}
exports.parseVersion = parseVersion;
function isGreaterThanTargetVersion(version, targetVersion) {
    if (!version || !targetVersion)
        return false;
    return (version.major > targetVersion.major) || ((version.major == targetVersion.major) && ((version.minor > targetVersion.minor) || ((version.minor == targetVersion.minor) && (version.build >= targetVersion.build))));
}
exports.isGreaterThanTargetVersion = isGreaterThanTargetVersion;
function getSdkVersion() {
    try {
        return parseVersion(Twilio.Chat.Client.version);
    }
    catch (e) { }
    try {
        return parseVersion(twilio_chat_1.Client.version);
    }
    catch (e) { }
    return undefined;
}
exports.getSdkVersion = getSdkVersion;
exports.simpleUrlRegexp = /^https?:\/\/[^\s]+/i;
exports.urlRegexp = RegExp("((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
    + "((?:(?:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}\\.)+" // named host
    + "(?:" // plus top level domain
    + "(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])"
    + "|(?:biz|b[abdefghijmnorstvwyz])"
    + "|(?:cat|com|coop|c[acdfghiklmnoruvxyz])"
    + "|d[ejkmoz]"
    + "|(?:edu|e[cegrstu])"
    + "|f[ijkmor]"
    + "|(?:gov|g[abdefghilmnpqrstuwy])"
    + "|h[kmnrtu]"
    + "|(?:info|int|i[delmnoqrst])"
    + "|(?:jobs|j[emop])"
    + "|k[eghimnrwyz]"
    + "|l[abcikrstuvy]"
    + "|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])"
    + "|(?:name|net|n[acefgilopruz])"
    + "|(?:org|om)"
    + "|(?:pro|p[aefghklmnrstwy])"
    + "|qa"
    + "|r[eouw]"
    + "|s[abcdeghijklmnortuvyz]"
    + "|(?:tel|travel|t[cdfghjklmnoprtvwz])"
    + "|u[agkmsyz]"
    + "|v[aceginu]"
    + "|w[fs]"
    + "|y[etu]"
    + "|z[amw]))"
    + "|(?:(?:25[0-5]|2[0-4]" // or ip address
    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
    + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
    + "|[1-9][0-9]|[0-9])))"
    + "(?:\\:\\d{1,5})?)" // plus option port number
    + "(\\/(?:(?:[a-zA-Z0-9\\;\\/\\?\\:\\@\\&\\=\\#\\~" // plus option query params
    + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
    + "(?:\\b|$)", "gi");
function copyMap(sourceMap) {
    if (!sourceMap)
        return undefined;
    var newMap = new Map();
    sourceMap.forEach(function (v, k) {
        newMap.set(k, v);
    });
    return newMap;
}
exports.copyMap = copyMap;
function isSameDate(date1, date2) {
    return date1.getDate() === date2.getDate() && date1.getMonth() == date2.getMonth() && date1.getFullYear() === date2.getFullYear();
}
exports.isSameDate = isSameDate;
function isToday(date) {
    var now = new Date();
    return isSameDate(now, date);
}
exports.isToday = isToday;
function isYesterday(date) {
    var yesterday = new Date();
    yesterday.setDate(date.getDate() - 1);
    return isSameDate(date, yesterday);
}
exports.isYesterday = isYesterday;
