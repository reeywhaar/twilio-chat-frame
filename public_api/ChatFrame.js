"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var react_redux_1 = require("react-redux");
require("./ChatFrame.less");
require("../themes/index.less");
var ChatModule_1 = require("../ChatModule");
var Store_1 = require("../Store");
var AppConfig = require("../state/ChatConfig");
var ChannelState = require("../state/ChannelState");
var SessionState = require("../state/SessionState");
var Globalization_1 = require("../Globalization");
var utils_1 = require("../utils/utils");
var ConnectedChannel_1 = require("../components/ConnectedChannel");
var defaultConfiguration = {
    language: "en-US",
    avatarCallback: undefined,
    channel: {
        windowControls: {
            visible: true,
            closeCallback: undefined
        },
        header: {
            visible: true,
            image: {
                visible: true,
                url: "https://www.twilio.com/marketing/bundles/company-brand/img/logos/red/twilio-mark-red.svg"
            },
            title: {
                visible: true,
                default: "Twilio Frame"
            }
        },
        message: {
            yourDefaultName: "",
            yourFriendlyNameOverride: true,
            theirDefaultName: "",
            theirFriendlyNameOverride: true,
            showReadStatus: true
        },
        visual: {
            colorTheme: "LightTheme",
            messageStyle: "Rounded",
            inputAreaStyle: "Line",
            override: undefined
        },
        showTypingIndicator: true,
        returnKeySendsMessage: true
    }
};
/**
 * Chat Frame.
 * Create a single instance using {@link ChatFrame.create create} method.
 *
 * Requires {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}
 */
var ChatFrame = (function () {
    /**
     * Constructor. Do not use.
     * @param {Twilio.Chat.Client} client Instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
     * @private
     */
    function ChatFrame(client) {
        var _this = this;
        this._channelsByContainer = new Map();
        this._containersByChannel = new Map();
        this.handleDocumentVisibilityChange = function (e) {
            _this.setAppActive(!document.hidden);
        };
        this.chatClient = client;
        document.addEventListener("visibilitychange", this.handleDocumentVisibilityChange);
    }
    /**
     * Creates Frame for Chat. In most cases you need only one {@link ChatFrame} instance - the method should be called only once.
     *
     * @param {Twilio.Chat.Client} client instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
     * @param {ChatFrame#ChatConfig} [userConfiguration] configuration object
     */
    ChatFrame.create = function (client, userConfiguration) {
        if (userConfiguration === void 0) { userConfiguration = {}; }
        ChatModule_1.ChatModule.init(function () { return Store_1.store.getState(); }, function (action) { return Store_1.store.dispatch(action); });
        var chatFrame = new ChatFrame(client);
        chatFrame.configure(userConfiguration);
        return chatFrame;
    };
    /**
     * Configures the Chat Frame. This mehtod should be called before loading any channels.
     * This method
     *
     * @param {ChatFrame#ChatConfig} userConfiguration Configuration object
     */
    ChatFrame.prototype.configure = function (userConfiguration) {
        var mergedConf = utils_1.merge(defaultConfiguration, userConfiguration);
        AppConfig.Actions.initConfig(mergedConf);
    };
    ChatFrame.prototype.setAppActive = function (isActive) {
        SessionState.Actions.setActive(isActive);
    };
    /**
     * Loads a channel to the provided container.
     * User must be a member of the channel.
     * You can only load channel into one container at a time.
     *
     * @param {String} containerId selector to specify the container where to load the channel.
     * @param {Channel} channel instance of a channel to load. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}
     *
     * @throws {Error} if minimum or maximum Chat SDK version criteria is not met
     * @throws {Error} if Channel is already loaded in another container
     * @throws {Error} if container is not found from DOM by selector
     * @throws {Error} if user is not a member of provided Channel
     */
    ChatFrame.prototype.loadChannel = function (containerId, channel) {
        var sdkVersion = utils_1.getSdkVersion();
        if (!utils_1.isGreaterThanTargetVersion(sdkVersion, utils_1.parseVersion(ChatFrame.MIN_SDK_VERSION))) {
            throw new Error("Twilio Chat SDK version criteria not met. Minimum required version is: " + ChatFrame.MIN_SDK_VERSION);
        }
        if (utils_1.isGreaterThanTargetVersion(sdkVersion, utils_1.parseVersion(ChatFrame.MAX_SDK_VERSION))) {
            throw new Error("Twilio Chat SDK version criteria not met. Maximum allowed SDK version is: " + ChatFrame.MAX_SDK_VERSION);
        }
        var existingContainer = this._containersByChannel.get(channel.sid);
        if (existingContainer && existingContainer != containerId) {
            throw new Error("Channel not loaded. Channel " + channel.sid + " is already loaded in container: " + existingContainer);
        }
        var container = document.querySelector(containerId);
        if (!container) {
            throw new Error("Channel not loaded. Unable to find container for selector: " + containerId);
        }
        if (channel.status !== "joined") {
            throw new Error("Channel not loaded. User is not a member of the provided channel");
        }
        var previousChannelSid = this._channelsByContainer.get(containerId);
        if (previousChannelSid) {
            console.log("Unloading previous channel from container: " + containerId);
            this.unloadChannelByContainer(containerId);
        }
        this._channelsByContainer.set(containerId, channel.sid);
        this._containersByChannel.set(channel.sid, containerId);
        SessionState.Actions.init(this.chatClient);
        ChannelState.Actions.init(channel);
        var rootClasses = [
            "Twilio",
            "Twilio-ChatFrame",
            "Twilio-" + AppConfig.current().channel.visual.colorTheme
        ];
        if (AppConfig.current().embedded) {
            rootClasses.push("Twilio-Embedded");
        }
        ReactDOM.unmountComponentAtNode(container);
        ReactDOM.render(React.createElement(react_redux_1.Provider, { store: Store_1.store },
            React.createElement("div", { className: rootClasses.join(" ") },
                React.createElement(ConnectedChannel_1.ConnectedChannel, { sid: channel.sid }))), container);
    };
    /**
     * Unload previously loaded channel from a container.
     * @param {String} containerId Selector to specify the container from where to unload the channel.
     */
    ChatFrame.prototype.unloadChannelByContainer = function (containerId) {
        var channelSid = this._channelsByContainer.get(containerId);
        if (!channelSid) {
            console.warn("Channel not registered for container: " + containerId);
            return;
        }
        var container = document.querySelector(containerId);
        ReactDOM.unmountComponentAtNode(container);
        ChannelState.Actions.unload(channelSid);
        this._channelsByContainer.delete(containerId);
        this._containersByChannel.delete(channelSid);
    };
    /**
     * Unload a channel by SID.
     * @param {String} channelSid SID of a previously loaded channel to unload.
     */
    ChatFrame.prototype.unloadChannelBySid = function (channelSid) {
        var container = this._containersByChannel.get(channelSid);
        if (!container) {
            console.warn("Container not found for channel: " + channelSid);
            return;
        }
        this.unloadChannelByContainer(container);
    };
    /**
     * Returns the instance of {@link Globalization} to set language strings.
     * @returns {Globalization}
     */
    ChatFrame.prototype.globalization = function () {
        return Globalization_1.Globalization.getInstance();
    };
    /**
     * Enable or disable message sending in a previously loaded channel.
     * @param {Boolean} enabled Whether the message sending is enabled or not
     * @param {String} channelSid SID of a loaded channel
     * @param {String} [reason] Reason to be shown inside input field when message sending is disabled
     */
    ChatFrame.prototype.setMessageSendingEnabled = function (enabled, channelSid, reason) {
        if (reason === void 0) { reason = undefined; }
        ChannelState.Actions.setMessageSendingEnabled(enabled, channelSid, reason);
    };
    return ChatFrame;
}());
/**
 * Minimum allowed Twilio Chat SDK Client version. Version of the provided SDK Client must be equal or greater than the minimum version.
 * @private
 */
ChatFrame.MIN_SDK_VERSION = "1.0.0";
/**
 * Maximum allowed Twilio Chat SDK Client version. Version of the provided SDK Client must be less than the maximum version.
 * @private
 */
ChatFrame.MAX_SDK_VERSION = "2.0.0";
exports.ChatFrame = ChatFrame;
