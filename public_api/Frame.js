"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ChatFrame_1 = require("./ChatFrame");
/**
 * Create Twilio Frame for Chat
 *
 * @param {Twilio.Chat.Client} chatClient Instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
 * @param {ChatFrame#ChatConfig} [userConfiguration] Configuration object
 */
function createChat(client, userConfiguration) {
    if (userConfiguration === void 0) { userConfiguration = undefined; }
    return ChatFrame_1.ChatFrame.create(client, userConfiguration);
}
exports.createChat = createChat;
