import "./ChatFrame.less";
import "../themes/index.less";
import { Client } from "twilio-chat";
import { Channel } from "twilio-chat/lib/channel";
import * as AppConfig from "../state/ChatConfig";
import { Globalization } from "../Globalization";
/**
 * Chat Frame.
 * Create a single instance using {@link ChatFrame.create create} method.
 *
 * Requires {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}
 */
export declare class ChatFrame {
    private chatClient;
    private _channelsByContainer;
    private _containersByChannel;
    /**
     * Creates Frame for Chat. In most cases you need only one {@link ChatFrame} instance - the method should be called only once.
     *
     * @param {Twilio.Chat.Client} client instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
     * @param {ChatFrame#ChatConfig} [userConfiguration] configuration object
     */
    static create(client: Client, userConfiguration?: AppConfig.ChatConfig): ChatFrame;
    /**
     * Configures the Chat Frame. This mehtod should be called before loading any channels.
     * This method
     *
     * @param {ChatFrame#ChatConfig} userConfiguration Configuration object
     */
    configure(userConfiguration: AppConfig.ChatConfig): void;
    /**
     * Constructor. Do not use.
     * @param {Twilio.Chat.Client} client Instance of the Twilio Chat Client. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}.
     * @private
     */
    private constructor(client);
    private handleDocumentVisibilityChange;
    setAppActive(isActive: boolean): void;
    /**
     * Minimum allowed Twilio Chat SDK Client version. Version of the provided SDK Client must be equal or greater than the minimum version.
     * @private
     */
    private static readonly MIN_SDK_VERSION;
    /**
     * Maximum allowed Twilio Chat SDK Client version. Version of the provided SDK Client must be less than the maximum version.
     * @private
     */
    private static readonly MAX_SDK_VERSION;
    /**
     * Loads a channel to the provided container.
     * User must be a member of the channel.
     * You can only load channel into one container at a time.
     *
     * @param {String} containerId selector to specify the container where to load the channel.
     * @param {Channel} channel instance of a channel to load. See {@link https://media.twiliocdn.com/sdk/js/chat/v1.0/docs/ Twilio Chat SDK}
     *
     * @throws {Error} if minimum or maximum Chat SDK version criteria is not met
     * @throws {Error} if Channel is already loaded in another container
     * @throws {Error} if container is not found from DOM by selector
     * @throws {Error} if user is not a member of provided Channel
     */
    loadChannel(containerId: string, channel: Channel): void;
    /**
     * Unload previously loaded channel from a container.
     * @param {String} containerId Selector to specify the container from where to unload the channel.
     */
    unloadChannelByContainer(containerId: string): void;
    /**
     * Unload a channel by SID.
     * @param {String} channelSid SID of a previously loaded channel to unload.
     */
    unloadChannelBySid(channelSid: string): void;
    /**
     * Returns the instance of {@link Globalization} to set language strings.
     * @returns {Globalization}
     */
    globalization(): Globalization;
    /**
     * Enable or disable message sending in a previously loaded channel.
     * @param {Boolean} enabled Whether the message sending is enabled or not
     * @param {String} channelSid SID of a loaded channel
     * @param {String} [reason] Reason to be shown inside input field when message sending is disabled
     */
    setMessageSendingEnabled(enabled: boolean, channelSid: string, reason?: string): void;
}
