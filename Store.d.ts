import { Store } from "redux";
import { ChatState } from "./state/ChatState";
export declare const store: Store<ChatState>;
