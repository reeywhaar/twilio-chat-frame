"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var default_1 = require("./localization/default");
var en_US_1 = require("./localization/en-US");
var et_EE_1 = require("./localization/et-EE");
var localizedStrings = function (language) {
    switch (language) {
        case "en-US": return en_US_1.default;
        case "et-EE": return et_EE_1.default;
    }
    return {};
};
/**
 * String map to define localizable language strings
 * @typedef {Object} Globalization#StringMap
 * @property {string} [InputPlaceHolder] Text shown in the edit box when no content has been entered.
 * @property {string} [TypingIndicator] Text used for typing indicator, user name is denoted by {0}. For example "{0} is typing"
 * @property {string} [Connecting] Text shown on banner when Programmable Chat is connecting
 * @property {string} [Disconnected] Text shown on banner when Programmable Chat is disconnected
 * @property {string} [Read] Text below last message that has been read by other party
 * @property {string} [MessageSendingDisabled] Text shown if sending messages has been disabled
 * @property {string} [Today] Text shown in message list to group messages from today's date
 * @property {string} [Yesterday] Text shown in message list to group messages from yesterday's date
 */
var _jsDocStringMap = {};
/**
 * Language map to include strings for all defined languages.
 * @typedef {Object} Globalization#LanguageMap
 * @property {Globalization#StringMap} [*] String map for a language. Use language code as a property name.
 */
var _jsDocLanguageMap = {};
/**
 * Globalization class. Used to define translation for languages.
 */
var Globalization = (function () {
    /**
     * Private
     *
     * @ignore
     */
    function Globalization() {
        this.language = Globalization.DEFAULT_LANGUAGE;
        this.overrides = {};
    }
    /**
     * Returns a singleton instance of the class.
     * @returns {Globalization}
     */
    Globalization.getInstance = function () {
        if (!this.instance) {
            this.instance = new Globalization();
        }
        return this.instance;
    };
    /**
     * Sets the active language.
     *
     * @param {string} language The code of the langauge in use, for example: "en-us"
     * @memberof Globalization
     */
    Globalization.prototype.setLanguage = function (language) {
        this.language = language;
        this.invalidateStrings();
    };
    /**
     * Sets value for string key for given language.
     *
     * @param {string} key Key of the string map. See {@link Globalization#StringMap} for key values.
     * @param {string} value Localized value for the key
     * @param {string} [language=Globalization.DEFAULT_LANGUAGE] The code of the langauge in use, for example: "en-us"
     */
    Globalization.prototype.setString = function (key, value, language) {
        if (language === void 0) { language = Globalization.DEFAULT_LANGUAGE; }
        if (!this.overrides.hasOwnProperty(language))
            this.overrides[language] = {};
        this.overrides[language][key] = value;
        this.invalidateStrings();
    };
    /**
     * Sets the entire set of strings for given language.
     *
     * @param {Globalization#StringMap} strings String map for the language
     * @param {string} [language=Globalization.DEFAULT_LANGUAGE] The code of the langauge in use, for example: "en-us"
     */
    Globalization.prototype.setLanguageStrings = function (strings, language) {
        if (language === void 0) { language = Globalization.DEFAULT_LANGUAGE; }
        this.overrides[language] = strings;
        this.invalidateStrings();
    };
    /**
     * Sets the entire language map with all strings.
     *
     * @param {LanguageMap} languageStrings Map of all languages and strings
     * @memberof Globalization
     */
    Globalization.prototype.setAllStrings = function (languageStrings) {
        this.overrides = languageStrings;
        this.invalidateStrings();
    };
    /**
     * Gets a string map for currently active language.
     * Language tokens that are missing from the language will be initialized from default language.
     *
     * @returns {Globalization#StringMap}
     */
    Globalization.prototype.getStrings = function () {
        if (!this.strings)
            this.initializeStrings();
        return this.strings;
    };
    Globalization.prototype.initializeStrings = function () {
        var localLangOverrides = !!this.overrides && !!this.overrides[this.language] ? this.overrides && this.overrides[this.language] : {};
        this.strings = __assign({}, default_1.defaultStrings, localizedStrings(this.language), localLangOverrides);
    };
    Globalization.prototype.invalidateStrings = function () {
        this.strings = undefined;
    };
    return Globalization;
}());
/**
 * Default language code.
 * @readonly
 * @default
 */
Globalization.DEFAULT_LANGUAGE = "en-US";
exports.Globalization = Globalization;
/**
 * Gets a string map for currently active language.
 *
 * @returns {Globalization#StringMap}
 * @ignore
 */
function strings() {
    return Globalization.getInstance().getStrings();
}
exports.strings = strings;
