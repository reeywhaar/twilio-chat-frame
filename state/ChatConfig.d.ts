import { Action } from "redux";
import { Message } from "twilio-chat/lib/message";
export interface ColorConfig {
    background?: string;
    color?: string;
}
export interface InputAreaColorConfig extends ColorConfig {
    placeholder?: ColorConfig;
}
export interface MessageColorConfig {
    avatar?: ColorConfig;
    body?: ColorConfig;
    info?: ColorConfig;
}
export interface ChannelStyleConfig {
    header?: ColorConfig;
    inputArea?: InputAreaColorConfig;
    main?: ColorConfig;
    sendButton?: ColorConfig;
    incomingMessage?: MessageColorConfig;
    outgoingMessage?: MessageColorConfig;
    readStatus?: ColorConfig;
}
export interface WindowControls {
    readonly visible?: boolean;
    readonly closeCallback?: (sid: string) => void;
}
export interface HeaderConfig {
    visible?: boolean;
    image?: {
        visible?: boolean;
        url?: string;
    };
    title?: {
        visible?: boolean;
        default?: string;
    };
}
export interface ChannelVisualConfig {
    readonly colorTheme?: string;
    readonly messageStyle?: string;
    readonly inputAreaStyle?: string;
    readonly override?: ChannelStyleConfig;
}
export interface MessageConfig {
    yourDefaultName?: string;
    theirDefaultName?: string;
    yourFriendlyNameOverride?: boolean;
    theirFriendlyNameOverride?: boolean;
    showReadStatus?: boolean;
    renderCallback?: (message: Message, domElement: HTMLElement) => void;
}
export interface ChannelConfig {
    readonly windowControls?: WindowControls;
    readonly header?: HeaderConfig;
    readonly message?: MessageConfig;
    readonly visual?: ChannelVisualConfig;
    readonly returnKeySendsMessage?: boolean;
    readonly showTypingIndicator?: boolean;
}
export interface ChatConfig {
    readonly language?: string;
    readonly avatarCallback?: (identity: string) => string;
    readonly channel?: ChannelConfig;
    readonly embedded?: boolean;
}
export interface AppConfigAction extends Action {
    readonly payload: ChatConfig;
}
export declare function reduce(state: ChatConfig, action: AppConfigAction): ChatConfig;
export declare function current(): ChatConfig;
export declare function tryGet(propCallBack: () => Object): Object;
export declare class Actions {
    static initConfig(config: ChatConfig): void;
}
