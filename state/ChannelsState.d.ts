import * as ChannelState from "./ChannelState";
export interface ChannelsState {
    [sid: string]: ChannelState.ChannelState;
}
export declare function reduce(state: ChannelsState, action: ChannelState.ChannelAction): ChannelsState;
