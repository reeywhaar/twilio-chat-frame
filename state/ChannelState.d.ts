import { Action, Dispatch } from "redux";
import { Channel } from "twilio-chat/lib/channel";
import { Member } from "twilio-chat/lib/member";
import { Message } from "twilio-chat/lib/message";
import { Paginator } from "twilio-chat/lib/interfaces/paginator";
import { User } from "twilio-chat/lib/user";
export declare type MembersType = Map<string, MemberState>;
export interface MemberState {
    readonly source: Member;
    readonly friendlyName: string;
}
export interface MessageState {
    readonly isFromMe: boolean;
    readonly source: Message;
    readonly groupWithNext: boolean;
    readonly groupWithPrevious: boolean;
}
export interface ChannelState {
    readonly currentPaginator: Paginator<Message>;
    readonly inputText: string;
    readonly isLoadingMessages: boolean;
    readonly isLoadingMembers: boolean;
    readonly isMessageSendingEnabled: boolean;
    readonly lastConsumedMessageIndex: number;
    readonly listener: ChannelListener;
    readonly members: MembersType;
    readonly messages: Array<MessageState>;
    readonly messageSendingDisabledReason: string;
    readonly source: Channel;
    readonly typers: Array<MemberState>;
}
export interface InitChannelPayload {
    channel: Channel;
    identity: string;
    listener: ChannelListener;
}
export declare type ChannelActionPayload = InitChannelPayload | MembersType | Paginator<Message> | Message | MemberState | string;
export interface ChannelAction extends Action {
    payload?: any;
    meta: {
        channelSid: string;
    };
}
export declare const ACTION_ADDED_MESSAGE = "CHANNEL_ADD_MESSAGE";
export declare const ACTION_ADDED_MEMBER = "CHANNEL_ADD_MEMBER";
export declare const ACTION_USER_UPDATED = "CHANNEL_USER_UPDATED";
export declare const ACTION_INIT_CHANNEL = "CHANNEL_INIT";
export declare const ACTION_UNLOAD_CHANNEL = "CHANNEL_UNLOAD";
export declare const ACTION_INIT_CHANNEL_MEMBERS = "CHANNEL_INIT_MEMBERS";
export declare const ACTION_INPUT_CHANGE = "CHANNEL_INPUT_CHANGE";
export declare const ACTION_CONSUME_MESSAGE = "ACTION_CONSUME_MESSAGE";
export declare const ACTION_LOAD_CHANNEL_MESSAGES = "CHANNEL_LOAD_MESSAGES";
export declare const ACTION_REMOVED_MESSAGE = "CHANNEL_REMOVE_MESSAGE";
export declare const ACTION_REMOVED_MEMBER = "CHANNEL_REMOVE_MEMBER";
export declare const ACTION_UPDATED_MEMBER = "CHANNEL_UPDATED_MEMBER";
export declare const ACTION_UPDATED_MESSAGE = "CHANNEL_UPDATE_MESSAGE";
export declare const ACTION_TYPING_STARTED = "CHANNEL_TYPING_STARTED";
export declare const ACTION_TYPING_ENDED = "CHANNEL_TYPING_ENDED";
export declare const ACTION_ENABLE_MESSAGE_SENDING = "CHANNEL_ENABLE_MESSAGE_SENDING";
export declare function reduce(state: ChannelState, action: ChannelAction): ChannelState;
export declare class Actions {
    private static readonly MESSAGE_LOAD_COUNT;
    private static _dispatcher;
    static dispatcher: Dispatch<any>;
    static dispatchChannelAction(sid: string, type: string, payload: any): void;
    static init(channel: Channel): void;
    static unload(channelSid: string): void;
    static loadMessagesFromHistory(channel: ChannelState): boolean;
    static sendMessage(channel: ChannelState): void;
    static sendTyping(channel: ChannelState, body: string): void;
    static updateLastConsumedMessageIndex(channel: ChannelState, newIndex: number): void;
    static setMessageSendingEnabled(enabled: boolean, channelSid: string, reason: string): void;
}
export declare class ChannelListener {
    private _channel;
    private _listening;
    private _users;
    constructor(channel: Channel);
    start(): void;
    stop(): void;
    startListeningUser(user: User): void;
    stopListeningUser(user: User): void;
    private dispatchMessageOrMemberAction(type, payload);
    private handleMessageAdded;
    private handleMessageUpdated;
    private handleMessageRemoved;
    private handleMemberJoined;
    private handleMemberLeft;
    private handleMemberUpdated;
    private handleTypingStarted;
    private handleTypingEnded;
    private handleUserUpdated;
    isListening(): boolean;
}
