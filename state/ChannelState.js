"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ChatModule_1 = require("../ChatModule");
var utils_1 = require("../utils/utils");
var initialState = {
    currentPaginator: undefined,
    inputText: "",
    isLoadingMembers: false,
    isLoadingMessages: false,
    isMessageSendingEnabled: true,
    lastConsumedMessageIndex: 0,
    listener: undefined,
    members: new Map(),
    messages: [],
    messageSendingDisabledReason: undefined,
    source: undefined,
    typers: []
};
var UserListenerItem = (function () {
    function UserListenerItem() {
    }
    return UserListenerItem;
}());
exports.ACTION_ADDED_MESSAGE = "CHANNEL_ADD_MESSAGE";
exports.ACTION_ADDED_MEMBER = "CHANNEL_ADD_MEMBER";
exports.ACTION_USER_UPDATED = "CHANNEL_USER_UPDATED";
exports.ACTION_INIT_CHANNEL = "CHANNEL_INIT";
exports.ACTION_UNLOAD_CHANNEL = "CHANNEL_UNLOAD";
exports.ACTION_INIT_CHANNEL_MEMBERS = "CHANNEL_INIT_MEMBERS";
exports.ACTION_INPUT_CHANGE = "CHANNEL_INPUT_CHANGE";
exports.ACTION_CONSUME_MESSAGE = "ACTION_CONSUME_MESSAGE";
exports.ACTION_LOAD_CHANNEL_MESSAGES = "CHANNEL_LOAD_MESSAGES";
exports.ACTION_REMOVED_MESSAGE = "CHANNEL_REMOVE_MESSAGE";
exports.ACTION_REMOVED_MEMBER = "CHANNEL_REMOVE_MEMBER";
exports.ACTION_UPDATED_MEMBER = "CHANNEL_UPDATED_MEMBER";
exports.ACTION_UPDATED_MESSAGE = "CHANNEL_UPDATE_MESSAGE";
exports.ACTION_TYPING_STARTED = "CHANNEL_TYPING_STARTED";
exports.ACTION_TYPING_ENDED = "CHANNEL_TYPING_ENDED";
exports.ACTION_ENABLE_MESSAGE_SENDING = "CHANNEL_ENABLE_MESSAGE_SENDING";
function getLastConsumedIndex(members, state) {
    var lastConsumedIndex = Number.MAX_VALUE;
    members.forEach(function (element) {
        if (element.source.identity != ChatModule_1.ChatModule.state.session.client.user.identity) {
            lastConsumedIndex = Math.min(element.source.lastConsumedMessageIndex, lastConsumedIndex);
        }
    });
    return lastConsumedIndex;
}
function reduce(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case exports.ACTION_INIT_CHANNEL: {
            var payload = action.payload;
            return __assign({}, initialState, { source: payload.channel, listener: payload.listener });
        }
        case exports.ACTION_UNLOAD_CHANNEL: {
            if (state.listener) {
                state.listener.stop();
            }
            return __assign({}, initialState);
        }
        case exports.ACTION_INIT_CHANNEL_MEMBERS + "_PENDING": {
            return __assign({}, state, { isLoadingMembers: true });
        }
        case exports.ACTION_INIT_CHANNEL_MEMBERS + "_FULFILLED": {
            var members = action.payload;
            return __assign({}, state, { isLoadingMembers: false, members: members, lastConsumedMessageIndex: getLastConsumedIndex(members, state) });
        }
        case exports.ACTION_INIT_CHANNEL_MEMBERS + "_REJECTED": {
            return __assign({}, state, { isLoadingMembers: false });
        }
        case exports.ACTION_LOAD_CHANNEL_MESSAGES + "_PENDING": {
            return __assign({}, state, { isLoadingMessages: true });
        }
        case exports.ACTION_LOAD_CHANNEL_MESSAGES + "_REJECTED": {
            return __assign({}, state, { isLoadingMessages: false });
        }
        case exports.ACTION_LOAD_CHANNEL_MESSAGES + "_FULFILLED": {
            var paginator = action.payload;
            var newMessages = paginator.items.map(function (message) { return createMessageState(message, undefined, undefined, ChatModule_1.ChatModule.state.session.client.user.identity); });
            newMessages = newMessages.concat(state.messages);
            // make all messages take into account its next message and previous messages
            for (var i = 0; i < Math.min(paginator.items.length + 1, newMessages.length); i++) {
                newMessages[i] = createMessageState(newMessages[i].source, i < newMessages.length - 1 ? newMessages[i + 1].source : undefined, i > 0 ? newMessages[i - 1].source : undefined, ChatModule_1.ChatModule.state.session.client.user.identity);
            }
            return __assign({}, state, { isLoadingMessages: false, currentPaginator: paginator, messages: newMessages });
        }
        case exports.ACTION_ADDED_MESSAGE: {
            var message = action.payload;
            // update last message to take into account that it has now a new next message
            var newMessages = state.messages.slice();
            newMessages.push(createMessageState(message, undefined, undefined, ChatModule_1.ChatModule.state.session.client.user.identity));
            for (var i = Math.max(newMessages.length - 2, 0); i < newMessages.length; i++) {
                newMessages[i] = createMessageState(newMessages[i].source, i < newMessages.length - 1 ? newMessages[i + 1].source : undefined, i > 0 ? newMessages[i - 1].source : undefined, ChatModule_1.ChatModule.state.session.client.user.identity);
            }
            return __assign({}, state, { messages: newMessages });
        }
        case exports.ACTION_REMOVED_MESSAGE: {
            var message_1 = action.payload;
            var messageIndex = state.messages.findIndex(function (m) { return m.source.index === message_1.index; });
            var newMessages = state.messages.slice();
            newMessages.splice(messageIndex, 1);
            for (var i = Math.max(0, messageIndex - 1); i <= Math.min(messageIndex, newMessages.length - 1); i++) {
                newMessages[i] = createMessageState(newMessages[i].source, i < newMessages.length - 1 ? newMessages[i + 1].source : undefined, i > 0 ? newMessages[i - 1].source : undefined, ChatModule_1.ChatModule.state.session.client.user.identity);
            }
            return __assign({}, state, { messages: newMessages });
        }
        case exports.ACTION_UPDATED_MESSAGE: {
            var message_2 = action.payload;
            var messageIndex = state.messages.findIndex(function (m) { return m.source.index === message_2.index; });
            var newMessages = state.messages.slice();
            newMessages[messageIndex] = createMessageState(message_2, messageIndex < newMessages.length - 1 ? newMessages[messageIndex + 1].source : undefined, messageIndex > 0 ? newMessages[messageIndex - 1].source : undefined, ChatModule_1.ChatModule.state.session.client.user.identity);
            return __assign({}, state, { messages: newMessages });
        }
        case exports.ACTION_ADDED_MEMBER: {
            var member = action.payload;
            var updatedMembers = utils_1.copyMap(state.members);
            updatedMembers.set(member.source.identity, member);
            return __assign({}, state, { members: updatedMembers });
        }
        case exports.ACTION_REMOVED_MEMBER: {
            var member = action.payload;
            var updatedMembers = utils_1.copyMap(state.members);
            if (updatedMembers.delete(member.source.identity)) {
                return __assign({}, state, { members: updatedMembers });
            }
            return state;
        }
        case exports.ACTION_UPDATED_MEMBER: {
            var member = action.payload;
            return __assign({}, state, { lastConsumedMessageIndex: getLastConsumedIndex(state.members, state) });
        }
        case exports.ACTION_TYPING_STARTED: {
            var member = action.payload;
            var typers = state.typers.slice();
            typers.push(member);
            return __assign({}, state, { typers: typers });
        }
        case exports.ACTION_TYPING_ENDED: {
            var member_1 = action.payload;
            var newTypers = state.typers.filter(function (memberItem) { return memberItem.source.identity != member_1.source.identity; });
            return __assign({}, state, { typers: newTypers });
        }
        case exports.ACTION_INPUT_CHANGE: {
            var body = action.payload;
            return __assign({}, state, { inputText: body });
        }
        case exports.ACTION_USER_UPDATED: {
            var user = action.payload.user;
            var member = state.members.get(user.identity);
            var updatedMembers = utils_1.copyMap(state.members);
            updatedMembers.set(user.identity, createMemberState(member.source, user.friendlyName));
            return __assign({}, state, { members: updatedMembers });
        }
        case exports.ACTION_CONSUME_MESSAGE: {
        }
        case exports.ACTION_ENABLE_MESSAGE_SENDING: {
            return __assign({}, state, { isMessageSendingEnabled: action.payload.enabled, messageSendingDisabledReason: action.payload.reason, inputText: "" });
        }
        default:
            return state;
    }
}
exports.reduce = reduce;
var Actions = (function () {
    function Actions() {
    }
    Object.defineProperty(Actions, "dispatcher", {
        get: function () {
            return this._dispatcher || ChatModule_1.ChatModule.dispatch;
        },
        set: function (dispatcher) {
            this._dispatcher = dispatcher;
        },
        enumerable: true,
        configurable: true
    });
    Actions.dispatchChannelAction = function (sid, type, payload) {
        this.dispatcher({
            type: type,
            payload: payload,
            meta: {
                channelSid: sid
            }
        });
    };
    Actions.init = function (channel) {
        // if we had channel before, then stop its listener
        var currentChannel = ChatModule_1.ChatModule.state.channels[channel.sid];
        if (!!currentChannel && !!currentChannel.listener)
            currentChannel.listener.stop();
        var listener = new ChannelListener(channel);
        // init channel, set indentity and channel object
        this.dispatchChannelAction(channel.sid, exports.ACTION_INIT_CHANNEL, {
            channel: channel,
            listener: listener
        });
        // Dispatch an action to load members => Promise<MembersType>
        this.dispatchChannelAction(channel.sid, exports.ACTION_INIT_CHANNEL_MEMBERS, channel.getMembers()
            .then(function (members) {
            var channelMembers = new Map();
            members.forEach(function (member) {
                member.getUser()
                    .then(function (user) {
                    listener.startListeningUser(user);
                });
            });
            return Promise.all(members.map(function (member) { return member.getUserDescriptor()
                .then(function (descriptor) {
                channelMembers.set(member.identity, createMemberState(member, descriptor.friendlyName));
            }); }))
                .then(function () { return channelMembers; });
        }));
        // Dispatch an action to load messages => Promise<Paginator<Message>>
        this.dispatchChannelAction(channel.sid, exports.ACTION_LOAD_CHANNEL_MESSAGES, channel.getMessages(this.MESSAGE_LOAD_COUNT));
    };
    Actions.unload = function (channelSid) {
        this.dispatchChannelAction(channelSid, exports.ACTION_UNLOAD_CHANNEL, undefined);
    };
    Actions.loadMessagesFromHistory = function (channel) {
        if (!channel.currentPaginator)
            return false;
        if (!channel.currentPaginator.hasPrevPage || channel.isLoadingMessages)
            return false;
        // Dispatch an action to load previous page => Promise<Paginator<Message>>
        this.dispatchChannelAction(channel.source.sid, exports.ACTION_LOAD_CHANNEL_MESSAGES, channel.currentPaginator.prevPage());
        return true;
    };
    Actions.sendMessage = function (channel) {
        channel.source.sendMessage(channel.inputText.trim());
        this.dispatchChannelAction(channel.source.sid, exports.ACTION_INPUT_CHANGE, "");
    };
    Actions.sendTyping = function (channel, body) {
        if (body !== "") {
            channel.source.typing();
        }
        this.dispatchChannelAction(channel.source.sid, exports.ACTION_INPUT_CHANGE, body);
    };
    Actions.updateLastConsumedMessageIndex = function (channel, newIndex) {
        channel.source.updateLastConsumedMessageIndex(newIndex);
    };
    Actions.setMessageSendingEnabled = function (enabled, channelSid, reason) {
        this.dispatchChannelAction(channelSid, exports.ACTION_ENABLE_MESSAGE_SENDING, {
            enabled: enabled,
            reason: reason
        });
    };
    return Actions;
}());
Actions.MESSAGE_LOAD_COUNT = 20;
exports.Actions = Actions;
var ChannelListener = (function () {
    function ChannelListener(channel) {
        var _this = this;
        this._listening = false;
        this.handleMessageAdded = function (message) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_ADDED_MESSAGE, message);
        };
        this.handleMessageUpdated = function (message) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_UPDATED_MESSAGE, message);
        };
        this.handleMessageRemoved = function (message) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_REMOVED_MESSAGE, message);
        };
        this.handleMemberJoined = function (member) {
            member.getUserDescriptor().then(function (descriptor) {
                _this.dispatchMessageOrMemberAction(exports.ACTION_ADDED_MEMBER, createMemberState(member, descriptor.friendlyName));
            });
            member.getUser().then(function (user) {
                _this.startListeningUser(user);
            });
        };
        this.handleMemberLeft = function (member) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_REMOVED_MEMBER, createMemberState(member, ""));
            member.getUser().then(function (user) {
                _this.stopListeningUser(user);
            });
        };
        this.handleMemberUpdated = function (member) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_UPDATED_MEMBER, member);
        };
        this.handleTypingStarted = function (member) {
            member.getUserDescriptor().then(function (descriptor) {
                _this.dispatchMessageOrMemberAction(exports.ACTION_TYPING_STARTED, createMemberState(member, descriptor.friendlyName));
            });
        };
        this.handleTypingEnded = function (member) {
            member.getUserDescriptor().then(function (descriptor) {
                _this.dispatchMessageOrMemberAction(exports.ACTION_TYPING_ENDED, createMemberState(member, descriptor.friendlyName));
            });
        };
        this.handleUserUpdated = function (user) {
            _this.dispatchMessageOrMemberAction(exports.ACTION_USER_UPDATED, { user: user });
        };
        this._users = new Map();
        this._channel = channel;
        this.start();
    }
    ChannelListener.prototype.start = function () {
        if (this._listening)
            return;
        this._channel.addListener("messageAdded", this.handleMessageAdded);
        this._channel.addListener("messageRemoved", this.handleMessageRemoved);
        this._channel.addListener("messageUpdated", this.handleMessageUpdated);
        this._channel.addListener("memberJoined", this.handleMemberJoined);
        this._channel.addListener("memberLeft", this.handleMemberLeft);
        this._channel.addListener("typingStarted", this.handleTypingStarted);
        this._channel.addListener("typingEnded", this.handleTypingEnded);
        this._channel.addListener("memberUpdated", this.handleMemberUpdated);
        this._listening = true;
    };
    ChannelListener.prototype.stop = function () {
        var _this = this;
        if (!this._listening)
            return;
        this._channel.removeListener("messageAdded", this.handleMessageAdded);
        this._channel.removeListener("messageRemoved", this.handleMessageRemoved);
        this._channel.removeListener("messageUpdated", this.handleMessageUpdated);
        this._channel.removeListener("memberJoined", this.handleMemberJoined);
        this._channel.removeListener("memberLeft", this.handleMemberLeft);
        this._channel.removeListener("typingStarted", this.handleTypingStarted);
        this._channel.removeListener("typingEnded", this.handleTypingEnded);
        this._channel.removeListener("memberUpdated", this.handleMemberUpdated);
        this._users.forEach(function (listener) {
            _this.stopListeningUser(listener.user);
        });
        this._listening = false;
    };
    ChannelListener.prototype.startListeningUser = function (user) {
        var _this = this;
        if (this._users.has(user.identity))
            return;
        var listener = new UserListenerItem();
        listener.user = user;
        listener.callback = function () {
            _this.handleUserUpdated(user);
        };
        user.addListener("updated", listener.callback);
        this._users.set(user.identity, listener);
    };
    ChannelListener.prototype.stopListeningUser = function (user) {
        if (!this._users.has(user.identity))
            return;
        var listener = this._users.get(user.identity);
        listener.user.removeListener("updated", listener.callback);
    };
    ChannelListener.prototype.dispatchMessageOrMemberAction = function (type, payload) {
        Actions.dispatchChannelAction(this._channel.sid, type, payload);
    };
    ChannelListener.prototype.isListening = function () {
        return this._listening;
    };
    return ChannelListener;
}());
exports.ChannelListener = ChannelListener;
// Helper functions
function createMessageState(message, nextMessage, previousMessage, myIdentity) {
    var groupWithNext = !!nextMessage && message.author === nextMessage.author;
    var groupWithPrevious = !!previousMessage && message.author === previousMessage.author;
    return {
        isFromMe: message.author === myIdentity,
        isFromAdmin: /^admin-\d*/.test(message.author),
        source: message,
        groupWithNext: groupWithNext,
        groupWithPrevious: groupWithPrevious,
    };
}
function createMemberState(member, friendlyName) {
    return {
        source: member,
        friendlyName: friendlyName
    };
}
