"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ChatModule_1 = require("../ChatModule");
var initialState = {
    connectionState: "",
    client: undefined,
    listener: undefined,
    isActive: true
};
exports.ACTION_INIT_SESSION = "SESSION_INIT";
exports.ACTION_CLIENT_CONNECTION = "SESSION_CLIENT_CONNECTION";
exports.ACTION_APP_ACTIVE = "SESSION_APP_ACTIVE";
function reduce(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case exports.ACTION_INIT_SESSION:
            var client = action.payload;
            if (state.listener) {
                state.listener.stop();
            }
            var listener = new ClientListener(client);
            return __assign({}, initialState, { client: client, connectionState: client.connectionState, listener: listener });
        case exports.ACTION_CLIENT_CONNECTION:
            var connectionState = action.payload;
            return __assign({}, state, { connectionState: connectionState });
        case exports.ACTION_APP_ACTIVE:
            return __assign({}, state, { isActive: action.payload });
        default:
            return state;
    }
}
exports.reduce = reduce;
var Actions = (function () {
    function Actions() {
    }
    Object.defineProperty(Actions, "dispatcher", {
        get: function () {
            return this._dispatcher || ChatModule_1.ChatModule.dispatch;
        },
        set: function (dispatcher) {
            this._dispatcher = dispatcher;
        },
        enumerable: true,
        configurable: true
    });
    Actions.init = function (client) {
        this.dispatcher({
            type: exports.ACTION_INIT_SESSION,
            payload: client
        });
    };
    Actions.dispatchConnectionState = function (connectionState) {
        this.dispatcher({
            type: exports.ACTION_CLIENT_CONNECTION,
            payload: connectionState
        });
    };
    Actions.setActive = function (isActive) {
        this.dispatcher({
            type: exports.ACTION_APP_ACTIVE,
            payload: isActive
        });
    };
    return Actions;
}());
exports.Actions = Actions;
var ClientListener = (function () {
    function ClientListener(client) {
        this._listening = false;
        this.handleConnectionStateChanged = function (connectionState) {
            Actions.dispatchConnectionState(connectionState);
        };
        this._client = client;
        this.start();
    }
    ClientListener.prototype.start = function () {
        if (this._listening)
            return;
        this._client.addListener("connectionStateChanged", this.handleConnectionStateChanged);
        this._listening = true;
    };
    ClientListener.prototype.stop = function () {
        if (!this._listening)
            return;
        this._client.removeListener("connectionStateChanged", this.handleConnectionStateChanged);
        this._listening = false;
    };
    ClientListener.prototype.isListening = function () {
        return this._listening;
    };
    return ClientListener;
}());
exports.ClientListener = ClientListener;
function current() {
    return ChatModule_1.ChatModule.state.session;
}
exports.current = current;
